﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DemoService.Domains.Models;
using DemoService.Resources;

namespace DemoService.Mapping
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<SavePostResource, Post>();
            CreateMap<SaveTagResource, Tag>();
        }
    }
}
