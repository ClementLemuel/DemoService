﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DemoService.Domains.Models;
using DemoService.Resources;

namespace DemoService.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Post, PostResource>();
            CreateMap<SinglePost, SinglePostResource>()
                .ForMember(s => s.PostTags, c => c.MapFrom(m => m.PostTags));
            CreateMap<Post, SinglePostResource>()
                .ForMember(s => s.PostTags, c => c.MapFrom(m => m.Tags));

            CreateMap<Tag, TagResource>();
            CreateMap<TagDetails, SingleTagResource>()
                .ForMember(s => s.TagPosts, c => c.MapFrom(m => m.TagPosts));
            CreateMap<Post, SinglePostResource>();
        }
    }
}
