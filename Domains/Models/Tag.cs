﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoService.Domains.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Label { get; set; }

        public virtual ICollection<PostTag> Posts { get; set; }
    }

    public class TagDetails : Tag
    { 
        public ICollection<Post> TagPosts { get; set; }
    }
}
