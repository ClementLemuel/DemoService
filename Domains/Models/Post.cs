﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoService.Domains.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public virtual ICollection<PostTag> Tags { get; set; }
    }

    public class SinglePost : Post
    {
        public ICollection<Tag> PostTags { get; set; }
    }
}
