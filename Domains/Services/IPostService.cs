﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;
using DemoService.Domains.Services.Communication;

namespace DemoService.Domains.Services
{
    public interface IPostService
    {
        Task<IEnumerable<Post>> ListAsync();
        Task<PostResponse> SaveAsync(Post post);
        Task<SinglePost> GetSinglePost(int postId);
        Task<PostResponse> UpdateAsync(int id, Post post);
        Task<PostResponse> DeleteAsync(int id);

    }
}
