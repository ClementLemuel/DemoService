﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;
using DemoService.Domains.Services.Communication;

namespace DemoService.Domains.Services
{
    public interface ITagService
    {
        Task<IEnumerable<Tag>> ListAsync();
        Task<TagResponse> SaveAsync(Tag tag);
        Task<TagDetails> GetTagDetails(int tagId);
        Task<TagResponse> UpdateAsync(int id, Tag tag);
        Task<TagResponse> DeleteAsync(int id);
    }
}
