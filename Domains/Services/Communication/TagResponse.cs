﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;

namespace DemoService.Domains.Services.Communication
{
    public class TagResponse : BaseResponse
    {
        public Tag Tag { get; private set; }

        private TagResponse(bool success, string message, Tag tag) : base(success, message)
        {
            Tag = tag;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="Tag">Saved tag.</param>
        /// <returns>Response.</returns>
        public TagResponse(Tag tag) : this(true, string.Empty, tag)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public TagResponse(string message) : this(false, message, null)
        { }
    }
}
