﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;

namespace DemoService.Domains.Repositories
{
    public interface ITagRepository
    {
        Task<IEnumerable<Tag>> ListAsync();
        Task AddSync(Tag tag);
        Task<TagDetails> GetTagPosts(int postId);
        Task<Tag> FindByIdAsync(int tagId);
        void Update(Tag tag);
        void Remove(Tag tag);
    }
}
