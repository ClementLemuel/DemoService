﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;

namespace DemoService.Domains.Repositories
{
    public interface IPostRepository
    {
        Task<IEnumerable<Post>> ListAsync();
        Task AddSync(Post post);
        Task<SinglePost> GetSinglePost(int postId);
        Task<Post> FindByIdAsync(int postId);
        void Update(Post post);
        void Remove(Post post);
    }
}
