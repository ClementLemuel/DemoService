﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;

namespace DemoService.Resources
{
    public class PostResource
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class SavePostResource
    {
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [MaxLength(int.MaxValue)]
        public string Content { get; set; }

        [Required]
        public ICollection<PostTag> Tags { get; set; }
    }

    public class SinglePostResource
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public ICollection<TagResource> PostTags { get; set; }
    }

    public class TagResource
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }

    public class SaveTagResource
    {
        [Required]
        [MaxLength(50)]
        public string Label { get; set; }

        public ICollection<PostTag> Posts { get; set; }
    }

    public class SingleTagResource
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public ICollection<PostResource> TagPosts { get; set; }
    }
}
