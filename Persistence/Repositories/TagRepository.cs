﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;
using DemoService.Domains.Repositories;
using DemoService.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace DemoService.Persistence.Repositories
{
    public class TagRepository :  BaseRepository, ITagRepository
    {
        public TagRepository(AppDbContext context) : base(context) { }

        public async Task AddSync(Tag tag)
        {
            await _context.Tags.AddAsync(tag);
        }

        public async Task<IEnumerable<Tag>> ListAsync()
        {
            return await _context.Tags.ToListAsync();
        }

        public async Task<TagDetails> GetTagPosts(int tagId)
        {
            var tag = await _context.Tags.Include(t => t.Posts)
                .ThenInclude(u => u.Post)
                .FirstOrDefaultAsync(t => t.Id == tagId);

            TagDetails tagDetail = null;
            if (tag != null)
            {
                tagDetail = new TagDetails();
                tagDetail.Id = tag.Id;
                tagDetail.Label = tag.Label;
                tagDetail.Posts = tag.Posts;
                tagDetail.TagPosts = tag.Posts.Select(t => t.Post).ToList();
                return tagDetail;
            }
            return tagDetail;
        }

        public async Task<Tag> FindByIdAsync(int tagId)
        {
            return await _context.Tags.Include(t => t.Posts).FirstOrDefaultAsync(t => t.Id == tagId);
        }

        public void Update(Tag tag)
        {
            _context.Tags.Update(tag);
        }

        public void Remove(Tag tag)
        {
            _context.Tags.Remove(tag);
        }
    }
}
