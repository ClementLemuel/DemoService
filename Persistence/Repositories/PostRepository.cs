﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DemoService.Domains.Models;
using DemoService.Domains.Repositories;
using DemoService.Persistence.Contexts;

namespace DemoService.Persistence.Repositories
{
    public class PostRepository : BaseRepository, IPostRepository
    {
        public PostRepository(AppDbContext context) : base(context) { }

        public async Task<IEnumerable<Post>> ListAsync()
        {
            return await _context.Posts.Include(b => b.Tags).ToListAsync();
        }

        public async Task<SinglePost> GetSinglePost(int postId)
        {
            var post = await _context.Posts.Include(p => p.Tags)
                .ThenInclude(q => q.Tag)
                .FirstOrDefaultAsync(p => p.Id == postId);

            SinglePost singlePost = null; 
            if (post != null)
            {
                singlePost = new SinglePost();
                singlePost.Id = post.Id;
                singlePost.Title = post.Title;
                singlePost.Content = post.Content;
                singlePost.Tags = post.Tags;
                singlePost.PostTags = post.Tags.Select(p => p.Tag).ToList();
                return singlePost;
            }
            return singlePost;
        }

        public async Task AddSync(Post post)
        {
            await _context.Posts.AddAsync(post);
        }

        public async Task<Post> FindByIdAsync(int postId)
        {
            return await _context.Posts.Include(p => p.Tags).FirstOrDefaultAsync(p => p.Id == postId);
        }

        public void Update(Post post)
        {
            _context.Posts.Update(post);
        }

        public void Remove(Post post)
        {
            _context.Posts.Remove(post);
        }
    }
}
