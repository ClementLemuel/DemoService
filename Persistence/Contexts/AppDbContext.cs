﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DemoService.Domains.Models;

namespace DemoService.Persistence.Contexts
{
    public class AppDbContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTag> PostTags { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Post>().ToTable("Posts");
            builder.Entity<Post>().HasKey(p => p.Id);
            builder.Entity<Post>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Post>().Property(p => p.Title).IsRequired().HasMaxLength(50);
            builder.Entity<Post>().Property(p => p.Content).HasMaxLength(int.MaxValue);

            builder.Entity<Post>().HasData
            (
                new Post { Id = 100, Title = "Fruits and Vegetables" }, // Id set manually due to in-memory provider
                new Post { Id = 101, Title = "Dairy", Content = "Milk, Cheese, and Cream!" }
            );

            builder.Entity<Tag>().ToTable("Tags");
            builder.Entity<Tag>().HasKey(p => p.Id);
            builder.Entity<Tag>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Tag>().Property(p => p.Label).IsRequired().HasMaxLength(50);

            builder.Entity<Tag>().HasData
            (
                new Tag { Id = 1, Label = "Healthy Lifestyle" }, // Id set manually due to in-memory provider
                new Tag { Id = 2, Label = "Cheese Lovers" }
            );

            builder.Entity<PostTag>().HasKey(p => new { p.PostId, p.TagId });
            builder.Entity<PostTag>()
                .HasOne(p => p.Post)
                .WithMany(p => p.Tags)
                .HasForeignKey(p => p.PostId);
            builder.Entity<PostTag>()
                .HasOne(p => p.Tag)
                .WithMany(p => p.Posts)
                .HasForeignKey(p => p.TagId);
        }
    }
}
