﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DemoService.Domains.Models;
using DemoService.Domains.Services;
using DemoService.Resources;
using DemoService.Extensions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DemoService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly IMapper _mapper;

        public PostsController(IPostService postService, IMapper mapper)
        {
            _postService = postService;
            _mapper = mapper;
        }

        // GET: api/<PostsController>
        [HttpGet]
        public async Task<IEnumerable<Resources.PostResource>> GetAllAsync()
        {
            var posts = await _postService.ListAsync();
            var resources = _mapper.Map<IEnumerable<Post>, IEnumerable<PostResource>>(posts);
            
            return resources;
        }

        // GET api/<PostsController>/5
        [HttpGet("{postId}")]
        public async Task<ActionResult<SinglePostResource>> Get(int postId)
        {
            var post = await _postService.GetSinglePost(postId);
            if (post == null)
                return NotFound();

            var resource = _mapper.Map<SinglePost, SinglePostResource>(post);
            return resource;
        }
        //public string Get(int id)
        //{
        //    return "value";
        //}



        // POST api/<PostsController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SavePostResource resource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var post = _mapper.Map<SavePostResource, Post>(resource);
            var result = await _postService.SaveAsync(post);

            if (!result.Success)
                return BadRequest(result.Message);

            var postResource = _mapper.Map<Post, PostResource>(result.Post);
            return Ok(postResource);
        }

        // PUT api/<PostsController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] SavePostResource resource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var post = _mapper.Map<SavePostResource, Post>(resource);
            var result = await _postService.UpdateAsync(id, post);

            if (!result.Success)
                return BadRequest(result.Message);

            //post = result.Post;

            //SinglePost singlePost = new SinglePost
            //{
            //    Id = post.Id,
            //    Title = post.Title,
            //    Content = post.Content,
            //    Tags = post.Tags,
            //    PostTags = post.Tags.Select(p => p.Tag).ToList()
            //};

            //var postResource = _mapper.Map<SinglePost, SinglePostResource>(singlePost);
            var postResource = _mapper.Map<Post, PostResource>(result.Post);
            return Ok(postResource);
        }

        // DELETE api/<PostsController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _postService.DeleteAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var postResource = _mapper.Map<Post, PostResource>(result.Post);
            return Ok(postResource);
        }
    }
}
