﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;
using DemoService.Domains.Repositories;
using DemoService.Domains.Services;
using DemoService.Domains.Services.Communication;

namespace DemoService.Services
{
    public class TagService : ITagService
    {
        private readonly ITagRepository _tagRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TagService(ITagRepository tagRepository, IUnitOfWork unitOfWork)
        {
            _tagRepository = tagRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<TagDetails> GetTagDetails(int tagId)
        {
            return await _tagRepository.GetTagPosts(tagId);
        }

        public async Task<IEnumerable<Tag>> ListAsync()
        {
            return await _tagRepository.ListAsync();
        }

        public async Task<TagResponse> SaveAsync(Tag tag)
        {
            try
            {
                await _tagRepository.AddSync(tag);
                await _unitOfWork.CompleteAsync();

                return new TagResponse(tag);
            }
            catch (Exception ex)
            {
                return new TagResponse($"An error occurred when saving the tag: {ex.Message}");
            }
        }

        public async Task<TagResponse> UpdateAsync(int tagId, Tag tag)
        {
            var existingTag = await _tagRepository.FindByIdAsync(tagId);

            if (existingTag == null)
                return new TagResponse("Tag not found.");

            existingTag.Label = tag.Label;
            existingTag.Posts.Clear();
            existingTag.Posts = tag.Posts;

            try
            {
                _tagRepository.Update(existingTag);
                await _unitOfWork.CompleteAsync();

                return new TagResponse(existingTag);
            }
            catch (Exception ex)
            {
                return new TagResponse($"An error occurred when updating the tag: {ex.Message}");
            }
        }

        public async Task<TagResponse> DeleteAsync(int tagId)
        {
            var existingTag = await _tagRepository.FindByIdAsync(tagId);

            if (existingTag == null)
                return new TagResponse("Tag not found.");

            try
            {
                _tagRepository.Remove(existingTag);
                await _unitOfWork.CompleteAsync();

                return new TagResponse(existingTag);
            }
            catch (Exception ex)
            {
                return new TagResponse($"An error occurred when deleting the tag: {ex.Message}");
            }
        }
    }
}
