﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoService.Domains.Models;
using DemoService.Domains.Repositories;
using DemoService.Domains.Services;
using DemoService.Domains.Services.Communication;

namespace DemoService.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PostService(IPostRepository postRepository, IUnitOfWork unitOfWork)
        {
            _postRepository = postRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Post>> ListAsync()
        {
            return await _postRepository.ListAsync();
        }

        public async Task<SinglePost> GetSinglePost(int postId)
        {
            return await _postRepository.GetSinglePost(postId);
        }

        public async Task<PostResponse> SaveAsync(Post post)
        {
            try
            {
                await _postRepository.AddSync(post);
                await _unitOfWork.CompleteAsync();

                return new PostResponse(post);
            }
            catch (Exception ex)
            {
                return new PostResponse($"An error occurred when saving the post: {ex.Message}");
            }
        }

        public async Task<PostResponse> UpdateAsync(int postId, Post post)
        {
            var existingPost = await _postRepository.FindByIdAsync(postId);

            if (existingPost == null)
                return new PostResponse("Post not found.");

            existingPost.Title = post.Title;
            existingPost.Content = post.Content;
            existingPost.Tags.Clear();
            existingPost.Tags = post.Tags;

            try
            {
                _postRepository.Update(existingPost);
                await _unitOfWork.CompleteAsync();

                return new PostResponse(existingPost);
            }
            catch (Exception ex)
            {
                return new PostResponse($"An error occurred when updating the post: {ex.Message}");
            }
        }

        public async Task<PostResponse> DeleteAsync(int postId)
        {
            var existingPost = await _postRepository.FindByIdAsync(postId);

            if (existingPost == null)
                return new PostResponse("Post not found.");

            try
            {
                _postRepository.Remove(existingPost);
                await _unitOfWork.CompleteAsync();

                return new PostResponse(existingPost);
            }
            catch (Exception ex)
            {
                return new PostResponse($"An error occurred when deleting the post: {ex.Message}");
            }
        }
    }
}
